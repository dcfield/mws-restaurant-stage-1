const DB_VERSION = 1;
const DB_NAME = "restaurantdb";

/**
 * Common database helper functions.
 */
class DBHelper {

  /**
   * Database URL.
   * Change this to restaurants.json file location on your server.
   */
  static get DATABASE_URL() {
    const port = 1337;// Change this to your server port
    return `http://localhost:${port}/restaurants`;
  }

  /**
   * Fetch all restaurants.
   */
  static fetchRestaurants(callback) {
    // Get IDB data first
    let cached = DBHelper.fetchAllRestaurantsFromIDB( (data) => {
      callback(null, data);
    //  return;
    });

    // If no IDB data then
    // fetch from server
    fetch(DBHelper.DATABASE_URL)
      .then(response => {
        return response.json();
      })
      .then((jsonData) => {
        DBHelper.storeResponseInIDB(jsonData);
        const restaurants = jsonData;
        callback(null, restaurants);
      })
      .catch(error => console.log(error));
  }

  /*
   * Get the restaurants from indexedDB if available
   */
  static fetchAllRestaurantsFromIDB(callback) {
    let DBOpenRequest = window.indexedDB.open("RestaurantDB", 2);
    let request = window.indexedDB.open(DB_NAME, DB_VERSION);

    request.onsuccess =  event => {
      var restaurants = [];

      let db = request.result;

      let transaction = db.transaction("restaurants");
      let objectStore = transaction.objectStore("restaurants");

      let items = objectStore.getAll();
      console.log(items);

      transaction.oncomplete = event => {
        callback(null, restaurants);
      };

      transaction.onerror = error => {
        console.log(error);
      };

      let cursorRequest = objectStore.openCursor();

      cursorRequest.onerror = error => {
        console.log(error);
      };

      cursorRequest.onsuccess = event => {
        let cursor = event.target.result;
        if (cursor) {
          restaurants.push(cursor.value);
          cursor.continue();
        }
      };
    };
  }

  static storeExists(db, name) {
    var exists = false;

    db.getSchema().stores.forEach(function (store) {
        if (store.name === name) {
            return exists = true;
        }
    });

    return exists;
  }

  /**
   * Store a json response into indexedDB
   */
  static storeResponseInIDB(response) {
    var request = window.indexedDB.open(DB_NAME, DB_VERSION);

    request.onupgradeneeded = event => {
      let db = event.target.result;
      console.log('hello');
      if(event.oldVersion < DB_VERSION){
        console.log('hello');
       // db.deleteObjectStore("restaurants");
      }

      // Create objectStore to hold restaurant data
      // Keypath will be
      let objectStore = db.createObjectStore("restaurants", {keyPath:"id"});

      // Create some indexes
      objectStore.createIndex("name", "name", { unique: false });
      objectStore.createIndex("address", "address", { unique: true });

      // Make sure objectStore created before
      // adding data to it
      objectStore.transaction.oncomplete = event => {
        // Store values in new objectStore
        let restaurantObjectStore = db.transaction("restaurants", "readwrite").objectStore("restaurants");
        response.forEach( restaurant => {
          restaurantObjectStore.add(restaurant);
        });
      };
    };

    request.onerror = event => {
      console.log(event.target.errorCode);
    };
  }

  /**
   * Fetch a restaurant by its ID.
   */
  static fetchRestaurantById(id, callback) {
    // fetch all restaurants with proper error handling.
    DBHelper.fetchRestaurants((error, restaurants) => {
      if (error) {
        callback(error, null);
      } else {
        const restaurant = restaurants.find(r => r.id == id);
        if (restaurant) { // Got the restaurant
          callback(null, restaurant);
        } else { // Restaurant does not exist in the database
          callback('Restaurant does not exist', null);
        }
      }
    });
  }

  /**
   * Fetch restaurants by a cuisine type with proper error handling.
   */
  static fetchRestaurantByCuisine(cuisine, callback) {
    // Fetch all restaurants  with proper error handling
    DBHelper.fetchRestaurants((error, restaurants) => {
      if (error) {
        callback(error, null);
      } else {
        // Filter restaurants to have only given cuisine type
        const results = restaurants.filter(r => r.cuisine_type == cuisine);
        callback(null, results);
      }
    });
  }

  /**
   * Fetch restaurants by a neighborhood with proper error handling.
   */
  static fetchRestaurantByNeighborhood(neighborhood, callback) {
    // Fetch all restaurants
    DBHelper.fetchRestaurants((error, restaurants) => {
      if (error) {
        callback(error, null);
      } else {
        // Filter restaurants to have only given neighborhood
        const results = restaurants.filter(r => r.neighborhood == neighborhood);
        callback(null, results);
      }
    });
  }



  /**
   * Fetch restaurants by a cuisine and a neighborhood with proper error handling.
   */
  static fetchRestaurantByCuisineAndNeighborhood(cuisine, neighborhood, callback) {
    // Fetch all restaurants
    DBHelper.fetchRestaurants((error, restaurants) => {
      if (error) {
        callback(error, null);
      } else {
        let results = restaurants;
        if (cuisine != 'all') { // filter by cuisine
          results = results.filter(r => r.cuisine_type == cuisine);
        }
        if (neighborhood != 'all') { // filter by neighborhood
          results = results.filter(r => r.neighborhood == neighborhood);
        }
        callback(null, results);
      }
    });
  }

  /**
   * Fetch all neighborhoods with proper error handling.
   */
  static fetchNeighborhoods(callback) {
    // Fetch all restaurants
    DBHelper.fetchRestaurants((error, restaurants) => {
      if (error) {
        callback(error, null);
      } else {
        // Get all neighborhoods from all restaurants
        const neighborhoods = restaurants.map((v, i) => restaurants[i].neighborhood);
        // Remove duplicates from neighborhoods
        const uniqueNeighborhoods = neighborhoods.filter((v, i) => neighborhoods.indexOf(v) == i);
        callback(null, uniqueNeighborhoods);
      }
    });
  }

  /**
   * Fetch all cuisines with proper error handling.
   */
  static fetchCuisines(callback) {
    // Fetch all restaurants
    DBHelper.fetchRestaurants((error, restaurants) => {
      if (error) {
        callback(error, null);
      } else {
        // Get all cuisines from all restaurants
        const cuisines = restaurants.map((v, i) => restaurants[i].cuisine_type);
        // Remove duplicates from cuisines
        const uniqueCuisines = cuisines.filter((v, i) => cuisines.indexOf(v) == i);
        callback(null, uniqueCuisines);
      }
    });
  }

  /**
   * Restaurant page URL.
   */
  static urlForRestaurant(restaurant) {
    return (`./restaurant?id=${restaurant.id}`);
  }

  /**
   * Restaurant image URL.
   */
  static imageUrlForRestaurant(restaurant) {
    return (`/dist/img/${restaurant.id}.jpg`);
  }

  static srcsetUrlForRestaurant(restaurant) {
    const srcset = (`/dist/img/${restaurant.id}-400w.jpg 400w, /dist/img/${restaurant.id}-600w.jpg 600w`);
    return srcset;
  }

  static imageAltTextForRestaurant(restaurant) {
    return (`${restaurant.name}`);
  }

  /**
   * Map marker for a restaurant.
   */
  static mapMarkerForRestaurant(restaurant, map) {
    const marker = new google.maps.Marker({
      position: restaurant.latlng,
      title: restaurant.name,
      url: DBHelper.urlForRestaurant(restaurant),
      map: map,
      animation: google.maps.Animation.DROP}
    );
    return marker;
  }

}
