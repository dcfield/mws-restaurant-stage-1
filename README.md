# Mobile Web Specialist Certification Course

An app that searches and retrieves data from nearby restaurants. This is a project that is part of the Mobile Web Specialist Certification Course.

## To start the application
1. Clone this repo and `cd` into it.
2. Run the application using `http-server` making sure it is running on localhost:8080
3. Clone https://github.com/dcfield/mws-restaurant-stage-2 and use `node server` to run it on localhost:1337. This is what will be called in our application.

### Prerequisites
What you will need for this to work.
1. http-server
2. node

## Authors
- David Caulfield