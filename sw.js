var STATIC_CACHE_NAME = 'my-cache-v1';
var urlsToCache = [
  '/dist/css/styles.css',
  '/js/main.js',
  '/dist/img/*',
  '/data/restaurants.json',
  '/js/restaurant_info.js',
 //'/js/dbhelper.js',
  '/index.html',
  '/restaurant.html'
];

var IMAGE_CACHE_NAME = 'img-cache-v1';

var allCaches = [
  STATIC_CACHE_NAME,
  IMAGE_CACHE_NAME
];

/**
* @description Open our cache once service worker installed
*/
self.addEventListener('install', (event) => {
  // Perform install steps
  event.waitUntil(
    caches.open(STATIC_CACHE_NAME)
      .then(function(cache) {
        console.log('Opened cache');
        return cache.addAll(urlsToCache);
      })
      .catch( (error) => {
        console.log(error);
      })
  );
});

self.addEventListener('activate', (event) => {
  console.log('activate');
  event.waitUntil(
    caches.keys().then( (cacheNames) => {
      return Promise.all(
        cacheNames.filter( (cacheName) => {
          return cacheName.includes('-cache-');
        }).map( (cacheName) => {
          return caches.delete(cacheName);
        })
      );
    })
  );
});

/**
* @description Return either the cached items, or the network request if cache doesn't exist
*/
self.addEventListener('fetch', (event) => {

  // for images
  // if(event.request.url.includes('/img/')){
  //   event.respondWith(servePhoto(event.request));
  //   return;
  // }

  /* for restaurant URL */
  if(event.request.url.includes('restaurant.html?id=')){
      const strippedurl = event.request.url.split('?')[0];

      event.respondWith(
          caches.match(strippedurl).then( (response) => {
              return response || fetch(event.response);
          })
      );
      return;
  }
  /* for all other URL */
  event.respondWith(
      caches.match(event.request).then( (response) => {
          return response || fetch(event.request);
      })
  );
});

function servePhoto(request) {
  // /dist/img/${restaurant.id}-400w.jpg 400w, /dist/img/${restaurant.id}-600w.jpg 600w
  var storageUrl = request.url.replace(/-\d+w\.jpg$/, '').replace('http://localhost:8080', '');
 //console.log(storageUrl);

  return caches.open(IMAGE_CACHE_NAME).then( (cache) => {
    return cache.match(storageUrl).then ( (response) => {
      if (response) return response;

      return fetch(request).then( (networkResponse) => {
        cache.put(storageUrl, networkResponse.clone());
        return networkResponse;
      });
    });
  });
}