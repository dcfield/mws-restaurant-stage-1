var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');


gulp.task('styles', () => {
	gulp.src('sass/**/*.scss')
		.pipe(sass().on('error',sass.logError))
		.pipe(autoprefixer({
			browsers: ['last 2 versions']
		}))
		.pipe(gulp.dest('./dist/css'));
});

gulp.task('copy-html', () => {
	gulp.src('./index.html')
		.pipe(gulp.dest('./dist'));
});

gulp.task('copy-images', () => {
	gulp.src('img/*')
		.pipe(gulp.dest('./dist/img'));
});

gulp.task('scripts', () => {
	gulp.src('js/**/*.js')
		.pipe(sourcemaps.init())
		.pipe(concat('all.js'))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('dist/js'));
});

gulp.task('scripts-dist', () => {
	gulp.src('js/**/*.js')
		.pipe(concat('all.js'))
		.pipe(gulp.dest('dist/js'));
});

gulp.task('img-dist', () => {
	gulp.src('./img/*')
		.pipe(imagemin({
			progressive:true,
			use: [pngquant()]
		}))
		.pipe(gulp.dest('dist/images'));
});

gulp.task('default', gulp.parallel(
	'styles',
	'copy-html',
	'copy-images',
	'scripts',
	'img-dist',
	(done) => {
		gulp.watch('./sass/**/*.scss', gulp.parallel('styles'));
		gulp.watch('./js/*.js', gulp.parallel('scripts-dist'));
		gulp.watch('./index.html',gulp.parallel('copy-html'));

		// browserSync.init({
		// 	server: './dist'
		// });

		return done();
}));

// Open in browser
/* browserSync.init({
     server: "./"
 });
 browserSync.stream();*/